﻿using System;
using System.Collections.Generic;

namespace Bee.Core
{
    public class EventAction : EventBase
    {
        private static readonly EventAction _ = null;

        static EventAction() { _ = new EventAction(); }

        public void ApplyImpl(string eventName, params object[] args)
        {
            var list = GetDelegateImpl(eventName);
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    try
                    {
                        list[i].Method.DynamicInvoke(args);
                    }
                    catch (Exception e)
                    {
                        EventAction.Apply("BEE_EXCEPTION_HANDLER", e);
                    }
                }
            }
        }

        public static void Apply(string eventName, params object[] args)
        {
            _.ApplyImpl(eventName, args);
        }

        public static void Add(string eventName, Delegate method, int priority = WeightedDelegate.NormalPriority)
        {
            _.AddImpl(eventName, method, priority);
        }

        public static void Clear(string eventName)
        {
            _.ClearImpl(eventName);
        }

        public static void ClearAll()
        {
            _.ClearAllImpl();
        }

        public static WeightedDelegateList GetDelegates(string eventName)
        {
            return _.GetDelegateImpl(eventName);
        }

        public static bool Has(string eventName)
        {
            return _.HasImpl(eventName);
        }

        public static void Remove(string eventName, Delegate method)
        {
            _.RemoveImpl(eventName, method);
        }

        public static ICollection<string> GetEventList()
        {
            return _.GetEvents();
        }
    }
}
