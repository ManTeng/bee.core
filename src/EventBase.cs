﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;

namespace Bee.Core
{
    /// <summary>
    /// Not thread safe
    /// </summary>
    public class EventBase
    {
        ConcurrentDictionary<string, WeightedDelegateList> BindingMap = null;
        protected Delegate _methodName = null;
        protected int _methodHashCode = -1;

        public EventBase()
        {
            BindingMap = new ConcurrentDictionary<string, WeightedDelegateList>(); 
        }

        protected void AddImpl(string eventName, Delegate func, int priority = WeightedDelegate.NormalPriority)
        {
            if (func==null)
            {
                return;
            }
            var methodInfo = func.Method;

            var func_type =  methodInfo.DeclaringType;

            int hash = (func_type.AssemblyQualifiedName + func_type.FullName + methodInfo.Name).GetHashCode();

            if (!BindingMap.ContainsKey(eventName))
            {
                BindingMap[eventName] = new WeightedDelegateList();

            }
            BindingMap[eventName].Add(new WeightedDelegate { Method = func, MethodHash = hash, Priority = priority });

        }

        protected void RemoveImpl(string eventName, Delegate func)
        {
            if (func==null)
            {
                return;
            }
            var methodInfo = func.Method;


            if (BindingMap.ContainsKey(eventName))
            {
                var func_type = methodInfo.DeclaringType;
                int hash = (func_type.AssemblyQualifiedName + func_type.FullName + methodInfo.Name).GetHashCode();
                WeightedDelegate obj = null;
                foreach (var action in BindingMap[eventName])
                {
                    if (action.MethodHash == hash)
                    {
                        obj = action;
                        break;
                    }
                }
                BindingMap[eventName].Remove(obj);
            }
        }

        protected bool HasImpl(string eventName)
        {
            return
                BindingMap.ContainsKey(eventName) && BindingMap[eventName].Count > 0 ?
                true : false;
        }

        protected WeightedDelegateList GetDelegateImpl(string eventName)
        {
            return BindingMap.ContainsKey(eventName) ? BindingMap[eventName] : null;
        }

        protected void ClearImpl(string eventName)
        {
            if (HasImpl(eventName))
            {
                BindingMap[eventName].Clear();
            }
        }

        protected void ClearAllImpl()
        {
            foreach (var key in BindingMap.Keys)
            {
                BindingMap[key].Clear();
            }
        }

        protected ICollection<string> GetEvents()
        {
            return BindingMap.Keys;
        }

    }
}
