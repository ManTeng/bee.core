﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Bee.Core
{

    public sealed class WeightedDelegate
    {
        public Delegate Method { get; set; }
        public int Priority { get; set; }
        public int MethodHash { get; set; }
        public const int NormalPriority = 999;

        public WeightedDelegate()
        {
            Priority = NormalPriority;
        }
    }


    public sealed class WeightedDelegateList : ICollection<WeightedDelegate>
    {
        private List<WeightedDelegate> _container = null;

        public WeightedDelegateList()
        {
            _container = new List<WeightedDelegate>();
        }

        public int Count
        {
            get
            {
                return _container.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public void Add(WeightedDelegate item)
        {
            int addPos = _container.Count;
            WeightedDelegate p = null;
            for (int i = 0; i < _container.Count; i++)
            {
                if (_container[i].Method == item.Method && _container[i].MethodHash == item.MethodHash)
                    return;
                if (item.Priority<=0)
                {
                    item.Priority = 1;
                }
                if (item.Priority != WeightedDelegate.NormalPriority && item.Priority < _container[i].Priority)
                {
                    if (p != null)
                    {
                        if (p.Priority > item.Priority)
                        {
                            p = item;
                            addPos = i;
                        }
                    }
                    else
                    {
                        p = _container[i];
                        addPos = i;
                    }
                }
            }
            if (addPos == _container.Count)
            {
                _container.Add(item);
            }
            else
            {
                _container.Insert(addPos, item);
            }
        }

        public void Clear()
        {
            _container.Clear();
        }

        public WeightedDelegate this[int num]
        {
            get
            {
                if (num >= 0 && num <= _container.Count)
                {
                    return _container[num];
                }
                throw new ArgumentOutOfRangeException("num");
            }
        }

        public bool Contains(WeightedDelegate item)
        {
            for (int i = 0; i < _container.Count; i++)
            {
                if (_container[i].Method == item.Method && _container[i].MethodHash == item.MethodHash)
                    return true;
            }
            return false;
        }

        public void CopyTo(WeightedDelegate[] array, int arrayIndex)
        {
            _container.CopyTo(array, arrayIndex);
        }

        public IEnumerator<WeightedDelegate> GetEnumerator()
        {
            return _container.GetEnumerator();
        }

        public bool Remove(WeightedDelegate item)
        {
            return _container.Remove(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _container.GetEnumerator();
        }
    }
}
