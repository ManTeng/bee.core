﻿using System;
using System.Collections.Generic;

namespace Bee.Core
{
    public class EventFilter : EventBase
    {

        private static readonly EventFilter _ = null;

        static EventFilter() { _ = new EventFilter(); }

        public static T Apply<T>(string eventName, params object[] args)
        {
            return _.ApplyImpl<T>(eventName, args);
        }

        public T ApplyImpl<T>(string eventName, params object[] args)
        {
            object result = default(T);
            var list = base.GetDelegateImpl(eventName);
            if (list == null)
            {
                return default(T);
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (args.Length == 0)
                {
                    result = list[i].Method.DynamicInvoke(args);
                }
                else
                {
                    args[0] = list[i].Method.DynamicInvoke(args);
                }

            }
            if (args.Length == 0)
            {
                return (T)result;
            }
            return (T)args[0];
        }

        public static void Add(string eventName, Delegate method, int priority = WeightedDelegate.NormalPriority)
        {
            _.AddImpl(eventName, method, priority);
        }

        public static void Clear(string eventName)
        {
            _.ClearImpl(eventName);
        }

        public static void ClearAll()
        {
            _.ClearAllImpl();
        }

        public static WeightedDelegateList GetDelegates(string eventName)
        {
            return _.GetDelegateImpl(eventName);
        }

        public static bool Has(string eventName)
        {
            return _.HasImpl(eventName);
        }

        public static void Remove(string eventName, Delegate method)
        {
            _.RemoveImpl(eventName, method);
        }


        public static ICollection<string> GetEventList()
        {
            return _.GetEvents();
        }
    }
}
